(ns ercoin-wallet.tx-test
  (:require [clojure.spec.alpha :as s]
            [clojure.test.check :as tc]
            [clojure.test.check.properties :as prop :include-macros true]
            [clojure.test.check.clojure-test :refer-macros [defspec]]
            [ercoin-wallet.tx :as tx]
            ))

(defspec tx-serialization
  100
  (prop/for-all [tx (s/gen ::tx/tx)]
                (= (tx/normalize tx)
                   (->> tx
                        tx/serialize
                        tx/deserialize
                        tx/normalize))))
