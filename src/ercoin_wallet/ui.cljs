(ns ercoin-wallet.ui
  (:require
   [antizer.reagent :as ant]
   [ercoin-wallet.account :as a]
   [ercoin-wallet.binary :as binary]
   [ercoin-wallet.i18n :refer [tr]]
   [ercoin-wallet.converter :as converter]
   [ercoin-wallet.timestamp :as timestamp]
   [reagent.core :as r]
   ))

(def label-width 8)

(def item-style
  {:label-col {:span label-width}
   :wrapper-col {:span 12}})

(defn tx-type-icon [type]
  (let [icon-type (case (keyword type)
                    :transfer "double-right"
                    :account "user"
                    :lock "lock"
                    :vote "file"
                    :burn "fire")]
    [ant/icon
     {:type icon-type
      :title type}]))

(defn timestamp-field [{:keys [default-picker-value
                               label
                               on-change
                               predefined-values
                               value]}]
  [ant/form-item
   (merge item-style
          {:label (or label (tr :tx/timestamp))})
   [ant/date-picker
    {:default-picker-value (when default-picker-value
                             (js/moment default-picker-value "X"))
     :show-time true
     :show-time-default-value (when default-picker-value
                                (js/moment default-picker-value "X"))
     :format "YYYY-MM-DD HH:mm:ss"
     :value (when value
              (js/moment value "X"))
     :on-change #(on-change (js/parseInt (.format % "X")))}]
   (for [[name generator] predefined-values]
     ^{:key name}
     [ant/button
      {:on-click #(on-change (generator))}
      name]
     )
   ])

(defn account-select [{:keys [allow-clear choices label on-change value]}]
  [ant/form-item
   (merge item-style
          {:label label})
   [ant/select
    {:allow-clear (if (nil? allow-clear)
                    false
                    allow-clear)
     :dropdown-match-select-width true
     :value (converter/encode binary/raw value)
     :on-change #(on-change (if (empty? %)
                              nil
                              (converter/decode binary/raw %)))}
    (for [account choices]
      [ant/select-option
       {:key (::a/address account)}
       (::a/label account)])
    ]])
