(ns ercoin-wallet.binary
  "Conversions between binary vectors and strings"
  (:require
   [alphabase.base58 :as base58]
   [alphabase.hex :as hex]
   [ercoin-wallet.uint :as uint]
   [ercoin-wallet.converter :refer [Converter]]
   [goog.crypt.base64 :as base64]))

(defrecord BinaryConverter [name key encoder decoder]
  Converter
  (encode [this vect] (encoder (or vect [])))
  (decode [this string] (decoder string)))

(def base58
  (BinaryConverter.
   "Base58"
   :base58
   #(base58/encode (clj->js %))
   #(-> %
        base58/decode
        js/Array.from
         js->clj)))

(def base64
  (BinaryConverter.
   "Base64"
   :base64
   #(-> %
        uint/vec->uint8arr
        base64/encodeByteArray)
   #(-> %
        base64/decodeStringToUint8Array
        uint/uint8arr->vec)))

(def hex
  (BinaryConverter.
   "Hex"
   :hex
   #(hex/encode (clj->js %))
   #(-> %
         hex/decode
         js/Array.from
         js->clj)))

(def raw
  (BinaryConverter.
   "Raw bytes"
   :raw
   #(clojure.string/join "," %)
   #(vec (map js/parseInt (clojure.string/split % ",")))))

(def utf8
  (BinaryConverter.
   "Text (UTF-8)"
   :utf8
   uint/utf8->str
   uint/str->utf8))

(def all-complete-converters
  [base58 base64 hex raw])

(def all-converters
  (conj all-complete-converters utf8))

(def all-converters-map
  (into {} (map (fn [conv] [(:key conv) conv]) all-converters)))

(defn get-converter [key]
  (get all-converters-map key))
