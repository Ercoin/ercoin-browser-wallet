(ns ercoin-wallet.i18n
  (:require
   [taoensso.tempura :as tempura]
   ))

;; Rules for making changes to English translations:
;; • If the meaning is changed in an important way, remove all related translations in other languages.
;; • If the meaning is changed in an unimportant way, place a comment “;; fuzzy" alongside all related translations in other languages.

(def translations
  {:en
   {:missing "MISSING TRANSLATION"
    :accounts
    {
     :add "Add"
     :add-account "Add account"
     :address "Address"
     :address-calculated-when-only-private-key "If private key is given, but no address, then address will be calculated from the private key."
     :actions "Actions"
     :balance "Balance"
     :empty-label-prohibition "Label must not be empty."
     :export-backup "Export backup"
     :import-backup "Import backup"
     :label "Label"
     :locked-until "Locked until"
     :merkle-proofs-not-supported "Merkle proofs are currently not supported."
     :new-account-generated-when-no-addresses "If neither address nor private key is given, then a new account will be generated."
     :private-key "Private key"
     :private-key-backup-advice "This account has associated private key stored. You may want to back it up first."
     :refresh "Refresh"
     :refresh-all "Refresh all"
     :removal-confirmation-question "Are you sure you want to remove the account?"
     :removal-not-from-blockchain "Removing an account from the wallet doesn’t mean deleting it from the blockchain."
     :remove "Remove"
     :show-private-key "Show private key"
     :validator-address "Validator address"
     :valid-until "Valid until"
     :watch-only-when-address-only "If address is given, but no private key, then account will be watch-only."
     }
    :sections
    {
     :accounts "Accounts"
     :create-transaction "Create transaction"
     :settings "Settings"
     :transactions "Transactions"
     }
    :settings
    {
     :binary-encoding "Binary encoding"
     :message-encoding "Message encoding"
     :rpc-endpoint "RPC endpoint"
     :unit "Unit"
     }
    :terms
    {
     :i-agree "I agree"
     :i-dont-agree "I don’t agree"
     :footer [:div
              "This is a simple in-browser wallet for "
              [:a {:href "https://ercoin.tech"} "Ercoin"]
              ", distributed under "
              [:a {:href "https://www.apache.org/licenses/LICENSE-2.0"} "Apache License 2.0"]
              ". You use it at your own risk. Its "
              [:a {:href "https://gitlab.com/Ercoin/ercoin-browser-wallet"} "source code"]
              " is available."
              ]
     :l10n-convenience-only "The translation below is provided only for convenience. Treat the original text in English (available below) as normative."
     ;; Befare when changing the English version of the terms. It will require the user to re-accept them.
     :terms [:div
             [:p
              "This wallet stores private keys in your browser. "
              "They are not supposed to be sent anywhere. "
              "However if the place from which you load the wallet gets compromised, malicious code may be injected which can steal your private keys. "
              "By default, it is your responsibility to ensure that you load the wallet from an uncompromised source. "
              "Take this into account especially when loading the wallet as online web page. "
              "When loading the wallet from a file, beware that some browsers use a shared local storage (in which private keys are saved) for all local files. "
              "This is not safe in a typical scenario. "
              ]
             [:p
              "Remember to create backup when you modify accounts’ list. "
              ]
             [:p
              "Licensed under the Apache License, Version 2.0 (the “License”); you may not use this wallet except in compliance with the License. "
              "You may obtain a copy of the License at "]
             [:p [:a {:href "https://www.apache.org/licenses/LICENSE-2.0"} "https://www.apache.org/licenses/LICENSE-2.0"]]
             [:p
              "Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. "
              "See the License for the specific language governing permissions and limitations under the License."]
             ]
     :text-changed-reevaluate "The text below has changed. Please reevaluate it."
     :title "Security considerations and legal terms"
     }
    :tx
    {
     :account "Account"
     :block-height "Block height"
     :error "Error"
     :fee "Fee"
     :fee-per-tx "Fee per tx"
     :fee-per-256-bytes "Fee per 256 bytes"
     :fee-per-account-day "Fee per account/day"
     :hash "Hash"
     :from "From"
     :locked-until "Locked until"
     :message "Message"
     :now "Now"
     :page "Page"
     :protocol "Protocol"
     :search "Search"
     :send "Send"
     :show "Show"
     :timestamp "Timestamp"
     :to "To"
     :transaction-binary "Transaction binary"
     :tx-committed "Committed transaction."
     :type "Type"
     :validator "Validator"
     :value "Value"
     }
    :ui
    {
     :amount "Amount"
     :generate "Generate"
     }
    :units
    {
     :centiercoin "centiercoin"
     :deciercoin "deciercoin"
     :ercoin "ercoin"
     :kiloercoin "kiloercoin"
     :megaercoin "megaercoin"
     :microercoin "microercoin"
     :miliercoin "miliercoin"
     }
    }
  :ru
   {:missing "ПЕРЕВОД ОТСУТСТВУЕТ"
    :accounts
    {
     :add "Добавить"
     :add-account "Добавить аккаунт"
     :address "Адрес"
     :address-calculated-when-only-private-key "Если указан приватный ключ но адрес не указан, тогда адрес будет вычислен из приватного ключа."
     :actions "Действия"
     :balance "Баланс"
     :empty-label-prohibition "Имя не должно быть пустым."
     :export-backup "Создать резервную копию"
     :import-backup "Загрузить резервную копию"
     :label "Имя"
     :locked-until "Заблокирован до"
     :merkle-proofs-not-supported "Доказательства Merkle в данный момент не поддерживаются."
     :new-account-generated-when-no-addresses "Новый аккаунт будет создан если не указан адрес или приватный ключ."
     :private-key "Приватный ключ"
     :private-key-backup-advice "Для данного аккаунта существует приватный ключ. Сделайте резервную копию."
     :refresh "Обновить"
     :refresh-all "Обновить все"
     :removal-confirmation-question "Вы точно хотите удалить аккаунт?"
     :removal-not-from-blockchain "Удаления аккаунта в кошельке не удаляет его из блокчейна."
     :remove "Удалить"
     :show-private-key "Показать приватный ключ"
     :validator-address "Адрес валидатора"
     :valid-until "Действительный до"
     :watch-only-when-address-only "Аккаунт будет в режиме только просмотр если для указаного адреса не предоставлен приватный ключ."
     }
    :sections
    {
     :accounts "Аккаунты"
     :create-transaction "Создать транзакцию"
     :settings "Настройки"
     :transactions "Транзакции"
     }
    :settings
    {
     :binary-encoding "Кодировка бинарных данных"
     :message-encoding "Кодировка сообщения"
     :rpc-endpoint "Адрес RPC"
     :unit "Единица измерения"
     }
    :terms
    {
     :i-agree "Я согласен"
     :i-dont-agree "Я не согласен"
     :footer [:div
              "Браузерная версия кошелька для "
              [:a {:href "https://ercoin.tech"} "Ercoin"]
              ", распространяется под лицензией "
              [:a {:href "https://www.apache.org/licenses/LICENSE-2.0"} "Apache License 2.0"]
              ". Использование на свой страх и риск. "
              [:a {:href "https://gitlab.com/Ercoin/ercoin-browser-wallet"} "Исходный код"]
              ]
     :l10n-convenience-only "The translation below is provided only for convenience. Treat the original text in English (available below) as normative."
     ;; Befare when changing the English version of the terms. It will require the user to re-accept them.
     :terms [:div
             [:p
              "Кошелек сохраняет приватные ключи в вашем браузере. "
              "Приватные ключи не передаются куда либо. "
              "Если устройство с которого вы использует кошелек скомпроментировано, ваши приватные ключи могут быть украдены. "
              "Это ваша ответственность быть уверенным что кошелек загружается с нескомпроментированого источника. "
              ]
             [:p
              "Не забудьте создать резервную копию после редактирования аккаунтов. "
              ]
             [:p
              "Лицензировано по лицензий Apache License, Version 2.0 (the “License”); вы можете использовать данный кошелек только в соответствии с лицензией. "
              "Вы можете получить копию лицензии на "]
             [:p [:a {:href "https://www.apache.org/licenses/LICENSE-2.0"} "https://www.apache.org/licenses/LICENSE-2.0"]]
             [:p
              "Если это не предусмотрено применимыми законами или не согласовано в письменной форме, программное обеспечение распространяется “КАК ЕСТЬ”, без гарантий и условий любого рода, явных или подразумеваемых. "
              ]
             ]
     :text-changed-reevaluate "Текст ниже был изменен. Пожалуйста пересмотрите его."
     :title "Предупреждения безопастности и юридические условия"
     }
    :tx
    {
     :account "Аккаунт"
     :block-height "Блок"
     :error "Ошибка"
     :fee "Комисcия"
     :fee-per-tx "Комиссия за транзакцию"
     :fee-per-256-bytes "Комиссия за 256 байт"
     :fee-per-account-day "Комиссия за аккаунт в день"
     :hash "Хеш"
     :from "От"
     :locked-until "Заблокирован до"
     :message "Сообщение"
     :now "Сейчас"
     :page "Страница"
     :protocol "Протокол"
     :search "Поиск"
     :send "Отправить"
     :show "Показать"
     :timestamp "Время"
     :to "Кому"
     :transaction-binary "Данные транзакции"
     :tx-committed "Транзакция создана."
     :type "Тип"
     :validator "Валидатор"
     :value "Количество"
     }
    :ui
    {
     :amount "Количество"
     :generate "Создать"
     }
    :units
    {
     :centiercoin "centiercoin"
     :deciercoin "deciercoin"
     :ercoin "ercoin"
     :kiloercoin "kiloercoin"
     :megaercoin "megaercoin"
     :microercoin "microercoin"
     :miliercoin "miliercoin"
     }
    }
  :uk
   {:missing "ПЕРЕКЛАД ВІДСУТНІЙ"
    :accounts
    {
     :add "Додати"
     :add-account "Додати аккаунт"
     :address "Адреса"
     :address-calculated-when-only-private-key "Якщо приватний ключ вказано але адреса відсутня, тоді адреса буде вирахувана з приватного ключа."
     :actions "Дії"
     :balance "Баланс"
     :empty-label-prohibition "Ім'я повинно бути вказано."
     :export-backup "Створити резервну копію"
     :import-backup "Завантажити резервну копію"
     :label "Ім'я"
     :locked-until "Заблокований до"
     :merkle-proofs-not-supported "Докази Merkle на разі не підтримуються."
     :new-account-generated-when-no-addresses "Новий аккаунт буде створено якщо не вказана адреса або приватний ключ."
     :private-key "Приватний ключ"
     :private-key-backup-advice "Для цього аккаунта існує приватний ключ. Зробіть резервну копію."
     :refresh "Оновити"
     :refresh-all "Оновити все"
     :removal-confirmation-question "Ви впевнені що хочете видалити аккаунт?"
     :removal-not-from-blockchain "Видалення аккаунта з гаманця не видаляе його з блокчейну."
     :remove "Видалити"
     :show-private-key "Показати приватний ключ"
     :validator-address "Адреса валідатора"
     :valid-until "Дійсний до"
     :watch-only-when-address-only "Аккаунт буде в режимі тільки перегляд якщо для вказаної адреси не надано приватного ключа."
     }
    :sections
    {
     :accounts "Аккаунти"
     :create-transaction "Створити транзакцію"
     :settings "Налаштування"
     :transactions "Транзакції"
     }
    :settings
    {
     :binary-encoding "Кодування бінарних даних"
     :message-encoding "Кодування повідомлення"
     :rpc-endpoint "Адреса RPC"
     :unit "Одиниця виміру"
     }
    :terms
    {
     :i-agree "Я згоден"
     :i-dont-agree "Я не згоден"
     :footer [:div
              "Браузерна версія гаманця для "
              [:a {:href "https://ercoin.tech"} "Ercoin"]
              ", поширюється за ліцензією "
              [:a {:href "https://www.apache.org/licenses/LICENSE-2.0"} "Apache License 2.0"]
              ". Використання на свій страх і ризик. "
              [:a {:href "https://gitlab.com/Ercoin/ercoin-browser-wallet"} "Програмний код"]
              ]
     :l10n-convenience-only "The translation below is provided only for convenience. Treat the original text in English (available below) as normative."
     ;; Befare when changing the English version of the terms. It will require the user to re-accept them.
     :terms [:div
             [:p
              "Гаманець зберігає приватні ключі в вашому браузері. "
              "Приватний ключі не передаються куди небудь за межі браузеру. "
              "Якщо пристрій з якого ви використовує гаманець скомпроментован, ваші приватні ключі можуть бути вкрадені. "
              "Це ваша відповідальність бути впевненим що гаманець завантажується з нескомпроментованого, джерела. "
              ]
             [:p
              "Не забудьте створити резервну копію після редагування акаунтів. "
              ]
             [:p
              "Ліцензовано за ліцензіею Apache License, Version 2.0 (the “License”); ви можете використовувати даний гаманець тільки відповідно до ліцензії. "
              "Ви можете отримати копію ліцензії на "]
             [:p [:a {:href "https://www.apache.org/licenses/LICENSE-2.0"} "https://www.apache.org/licenses/LICENSE-2.0"]]
             [:p
              "Якщо це не передбачено чинними законами або не узгоджене в письмовій формі, програмне забезпечення поширюється “ЯК Є”, без гарантій та умов будь-якого роду, неявних або інших. "
              ]
             ]
     :text-changed-reevaluate "Текст нижче був змінений. Будь ласка перегляньте його."
     :title "Попередження безпеки і юридичні умови"
     }
    :tx
    {
     :account "Аккаунт"
     :block-height "Блок"
     :error "Помилка"
     :fee "Комісcія"
     :fee-per-tx "Комісcія за транзакцію"
     :fee-per-256-bytes "Комісcія за 256 байт"
     :fee-per-account-day "Комісcія за аккаунт в день"
     :hash "Хеш"
     :from "Від"
     :locked-until "Заблоковано до"
     :message "Повідомлення"
     :now "Зараз"
     :page "Сторінка"
     :protocol "Протокол"
     :search "Пошук"
     :send "Відправити"
     :show "Показати"
     :timestamp "Час"
     :to "Кому"
     :transaction-binary "Дані транзакції"
     :tx-committed "Транзакція створена."
     :type "Тип"
     :validator "Валідатор"
     :value "Кількість"
     }
    :ui
    {
     :amount "Кількість"
     :generate "Створити"
     }
    :units
    {
     :centiercoin "centiercoin"
     :deciercoin "deciercoin"
     :ercoin "ercoin"
     :kiloercoin "kiloercoin"
     :megaercoin "megaercoin"
     :microercoin "microercoin"
     :miliercoin "miliercoin"
     }
    }
  })

(def tr-opts
  {:default-locale :en
   :dict translations})

(def preferred-languages
  (->> js/navigator.languages
       js->clj
       (map keyword)
       vec))

(defn tr
  ([resource-id]
   (tr preferred-languages resource-id nil))
  ([resource-id resource-args]
   (tr preferred-languages resource-id resource-args))
  ([languages resource-id resource-args]
   (tempura/tr tr-opts languages [resource-id] resource-args)))
