(ns ercoin-wallet.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
   [alandipert.storage-atom :refer [local-storage]]
   [alphabase.hex :as hex]
   [antizer.reagent :as ant]
   [cljs.core.async :refer [<!]]
   [clojure.spec.alpha :as s]
   [reagent.core :as r]
   [cljsjs.nacl-fast]
   [ercoin-wallet.account :as a]
   [ercoin-wallet.binary :as binary]
   [ercoin-wallet.converter :as converter]
   [ercoin-wallet.i18n :refer [tr]]
   [ercoin-wallet.tendermint :as tendermint]
   [ercoin-wallet.terms :as terms]
   [ercoin-wallet.timestamp :as timestamp]
   [ercoin-wallet.types :as types]
   [ercoin-wallet.tx :as tx]
   [ercoin-wallet.ui :as ui]
   [ercoin-wallet.uint :as uint]
   [ercoin-wallet.units :as units]))

;; State

(defonce accounts
  (local-storage (r/atom {}) :accounts))
(defonce rpc-endpoint
  (local-storage (r/atom "http://localhost:26657") :rpc-endpoint))
(defonce binary-encoding
  (local-storage (r/atom :base58) :binary-encoding))
(defonce unit
  (local-storage (r/atom :ercoin) :unit))
(defonce accepted-terms-checksum
  (local-storage (r/atom nil) :accepted-terms-checksum))
(defonce message-encoding
  (local-storage (r/atom :utf8) :message-encoding))
(defonce txs
  (r/atom '()))
(defonce active-section
  (r/atom "accounts"))

;; Tendermint communication

(defn fetch-account [address]
  (go
    (let [value (<! (tendermint/query
                     @rpc-endpoint
                     "account"
                     address))]
      (a/deserialize address value))))

(defn update-account! [{:keys [::a/address] :as data}]
  (swap! accounts
         #(update-in % [address] data)))

(defn put-account! [{:keys [::a/address] :as account}]
  (swap! accounts #(assoc % address account)))

(defn refresh-account! [{:keys [::a/address] :as account}]
  (go
    (let [fresh-account (<! (fetch-account address))]
      (put-account! (merge account fresh-account)))))

(defn refresh-accounts! []
  (doseq [account (vals @accounts)]
    (refresh-account! account)))

;; Converters

(defn binary-decode [string]
  (converter/decode (binary/get-converter @binary-encoding) string))

(defn binary-encode [vect]
  (converter/encode (binary/get-converter @binary-encoding) vect))

(defn message-decode [string]
  (converter/decode (binary/get-converter @message-encoding) string))

(defn message-encode [vect]
  (converter/encode (binary/get-converter @message-encoding) vect))

(defn unit-encode [value]
  (converter/encode (units/get-unit @unit) value))

(defn unit-decode [value-str]
  (converter/decode (units/get-unit @unit) value-str))

(defn amount-encode [amount]
  (apply str (unit-encode amount) " " (:symbol (units/get-unit @unit))))

(defn amount-encode-with-decimal [amount]
  (if (zero? (:resolution (units/get-unit @unit)))
    (amount-encode amount)
    (-> amount
        amount-encode
        (clojure.string/replace #"^(\d+) " "$1.0 "))))

(defn amount-decode [amount-str]
  (let [[amount-number-str unit-symbol] (clojure.string/split amount-str " ")]
    (converter/decode (units/get-unit-by-symbol unit-symbol) amount-number-str)))

;; Views

(defn binary [bin]
  [:code (binary-encode bin)])

(defn address-friendly [address]
  (when address
    (let [label (get-in @accounts [address ::a/label])]
      (or label [:code (binary-encode address)]))))

(defn accounts-vec
  ([]
   (accounts-vec (fn [account] true)))
  ([pred]
   (->> @accounts
        vals
        (filter pred)
        (sort-by (comp clojure.string/lower-case ::a/label)))))

(defn cell-renderer [fun]
  #(r/as-element (fun (js->clj %2))))

(def accounts-table-columns
  [;; TODO: Make labels editable.
   {:title (tr :accounts/label)
    :dataIndex ::a/label}
   {:title (tr :accounts/address)
    :render (cell-renderer (fn [tx] [:code (binary-encode (get tx "address"))]))}
   {:title (tr :accounts/balance)
    :render (cell-renderer (fn [tx] (unit-encode (get tx "balance"))))}
   {:title (tr :accounts/valid-until)
    :render (cell-renderer (fn [tx] (timestamp/format (get tx "valid-until"))))}
   {:title (tr :accounts/locked-until)
    :render (cell-renderer (fn [tx] (timestamp/format (get tx "locked-until"))))}
   {:title (tr :accounts/validator-address)
    :render (cell-renderer (fn [tx] [:code (binary-encode (get tx "validator-address"))]))}
   {:title (tr :accounts/actions)
    :render (cell-renderer
             (fn [tx]
               [:span
                [ant/button
                 {:icon "reload"
                  :title (tr :accounts/refresh)
                  :on-click (fn [] (refresh-account! (get @accounts (get tx "address"))))}]
                [ant/button
                 {:icon :delete
                  :title (tr :accounts/remove)
                  :type "danger"
                  :on-click (fn []
                              (let [modal-visible (r/atom true)]
                                (r/as-element
                                 (ant/modal-confirm
                                  {:title (tr :accounts/removal-confirmation-question)
                                   :content (r/as-element
                                             [:div
                                              (when (seq (get tx "private-key"))
                                                [:p (tr :accounts/private-key-backup-advice)])
                                              [:p (tr :accounts/removal-not-from-blockchain)]])
                                   :on-ok (fn [] (do (swap! accounts (fn [accounts] (dissoc accounts (get tx "address"))))
                                                     (reset! modal-visible false)))}))))}]
                [ant/button
                 {:icon "warning"
                  :title (tr :accounts/show-private-key)
                  :type "danger"
                  :on-click (fn []
                              (let [modal-visible (r/atom true)]
                                (r/as-element
                                 (ant/modal-info
                                  {:title (tr :accounts/private-key)
                                   :content (r/as-element
                                             [:div (binary-encode (get tx "private-key"))])
                                   :on-ok (fn [] (reset! modal-visible false))}))))}]
                ]))}
   ])

(defn accounts-table []
  [ant/table
   {:columns accounts-table-columns
    :dataSource (accounts-vec)
    :pagination false}])

(def txs-table-columns
  [
   {:title (tr :tx/block-height)
    :render (cell-renderer #(get-in % ["meta" "height"]))}
   {:title (tr :tx/hash)
    :render (cell-renderer (fn [tx] [:code (binary-encode (get-in tx ["meta" "hash"]))]))}
   {:title (tr :tx/type)
    :render (cell-renderer #(ui/tx-type-icon (get % "type")))}
   {:title (tr :tx/from)
    :render (cell-renderer #(address-friendly (get-in % ["meta" "from"])))}
   {:title (tr :tx/to)
    :render (cell-renderer #(address-friendly (get % "to")))}
   {:title (tr :tx/locked-until)
    :render (cell-renderer #(timestamp/format (or
                                               (get % "valid-until")
                                               (get % "locked-until"))))}
   {:title (tr :tx/value)
    :render (cell-renderer #(when-let [value (get % "value")]
                              (amount-encode value)))}
   {:title (tr :tx/fee)
    :render (cell-renderer #(amount-encode (get-in % ["meta" "fee"])))}
   {:title (tr :tx/message)
    :render (cell-renderer #(message-encode (get % "message")))}
   ])

(defn txs-table []
  [ant/table
   {:columns txs-table-columns
    :dataSource (map #(assoc % :meta (meta %))
                     @txs)
    :pagination false}])

(defn rpc-endpoint-field []
  [ant/form-item
   (merge ui/item-style
          {:label (tr :settings/rpc-endpoint)})
   [ant/input
    {:type :text
     :value @rpc-endpoint
     :on-change #(reset! rpc-endpoint (-> % .-target .-value))}]])

(defn binary-encoding-field []
  [ant/form-item
   (merge ui/item-style
          {:label (tr :settings/binary-encoding)})
   [ant/radio-group
    {:default-value (name @binary-encoding)
     :button-style "solid"
     :on-change #(reset! binary-encoding (-> % .-target .-value keyword))}
    (for [{key :key name :name} binary/all-complete-converters]
      ^{:key key}
      [ant/radio
       {:value (cljs.core/name key)}
       name])]])

(defn message-encoding-field []
  [ant/form-item
   (merge ui/item-style
          {:label (tr :settings/message-encoding)})
   [ant/radio-group
    {:default-value (name @message-encoding)
     :button-style "solid"
     :on-change #(reset! message-encoding (-> % .-target .-value keyword))}
    (for [{key :key name :name} binary/all-converters]
      ^{:key key}
      [ant/radio
       {:value (cljs.core/name key)}
       name])]])

(defn unit-field []
  [ant/form-item
   (merge ui/item-style
          {:label (tr :settings/unit)})
   [ant/select
    {:defaultValue @unit
     :on-select #(reset! unit (keyword %))}
    (for [{:keys [:name :key]} units/all]
      [ant/select-option
       {:key key}
       name])]])

(defn accounts-refresh-button []
  [ant/button
   {:icon :reload
    :on-click refresh-accounts!}
   (tr :accounts/refresh-all)])

(defn accounts-backup-str []
  (->> @accounts
       vals
       (map #(select-keys % [::a/label ::a/address ::a/private-key]))
       (into [])
       pr-str
       ))

(defn merge-accounts [account-1 account-2]
  (reduce
   (fn [account-acc key]
     (let [new-value (or (get account-1 key)
                         (get account-2 key))]
       (if new-value
         (assoc account-acc key new-value)
         account-acc)))
   account-1
   (keys account-2)))

(defn import-accounts [new-accounts]
  (swap!
   accounts
   (fn [old-accounts]
     (reduce
      (fn [accounts-acc account]
        (assoc accounts-acc (::a/address account) (merge-accounts (get old-accounts (::a/address account) {})
                                                                  account)))
      old-accounts
      new-accounts))))

(defn export-backup-button []
  ;; TODO: Do not pregenerate backup file.
  (let [blob (js/Blob. [(accounts-backup-str)] {:type "application/edn"})]
    [:a
     {:href (js/URL.createObjectURL blob)
      :download "ercoin-accounts.edn"}
     [ant/button
      (tr :accounts/export-backup)]]))

(defn import-backup-button []
  (let [reader (js/FileReader.)
        visible (r/atom false)
        new-accounts-str (r/atom nil)]
    (fn []
      (do
        (set! (.-onload reader)
              #(reset! new-accounts-str (.. % -target -result)))
        [:span
         [ant/button
          {:on-click #(reset! visible true)}
          (tr :accounts/import-backup)]
         [ant/modal
          ;; TODO: Clear file input when modal is closed.
          {:title (tr :accounts/import-backup)
           :on-ok #(do (reset! visible false)
                       (import-accounts (cljs.reader/read-string @new-accounts-str))
                       (reset! new-accounts-str nil))
           :on-cancel #(reset! visible false)
           :visible @visible}
          [ant/input
           {:type "file"
            :on-change #(.readAsText reader (aget (.. % -target -files) 0))}]]]))))

(defn amount-field [{:keys [label on-change value]}]
  (let [{:keys [:resolution :symbol]} (units/get-unit @unit)]
    [ant/form-item
     (merge ui/item-style
            {:label (or label (tr :ui/amount))})
     [ant/input-number
      {;; We force decimal display to avoid problem with dot entering.
       :formatter amount-encode-with-decimal
       :parser amount-decode
       :min 0
       :value value
       :max (dec (js/Math.pow 2 64))
       :on-change on-change}]]))

(defn generate-button [tx-atom generator]
  [ant/form-item
   {:wrapper-col {:offset ui/label-width}}
   [ant/button
    {:on-click #(reset! tx-atom (generator))}
    (tr :ui/generate)]])

(defn transfer-tx-form [tx-atom]
  [ant/form
   [ui/account-select
    {:label (tr :tx/from)
     :on-change #(swap! tx-atom (fn [tx] (assoc tx ::tx/from %)))
     :value (::tx/from @tx-atom)
     :choices (accounts-vec ::a/private-key)
     }]
   [ui/account-select
    {:label (tr :tx/to)
     :on-change #(swap! tx-atom (fn [tx] (assoc tx ::tx/to %)))
     :value (::tx/to @tx-atom)
     :choices (accounts-vec)
     }]
   [amount-field
    {:on-change #(swap! tx-atom (fn [tx] (assoc tx ::tx/value %)))
     :value (::tx/value @tx-atom)
     }]
   [ant/form-item
    (merge ui/item-style
           {:label (tr :tx/message)})
    [ant/input
     {:type :text
      :value (message-encode (::tx/message @tx-atom))
      :on-change #(swap! tx-atom
                         (fn [tx] (assoc tx
                                         ::tx/message
                                         (-> %
                                             .-target
                                             .-value
                                             message-decode))))}]]
   [ui/timestamp-field
    {:on-change #(swap! tx-atom (fn [tx] (assoc tx ::tx/timestamp %)))
     :value (::tx/timestamp @tx-atom)
     :predefined-values [[(tr :tx/now) timestamp/now]]}]])

(defn lock-tx-form [tx-atom]
  [:div
   [ui/timestamp-field
    {:default-picker-value (get-in @accounts [(::tx/from @tx-atom) ::a/locked-until])
     :on-change #(swap! tx-atom (fn [tx] (assoc tx ::tx/locked-until %)))
     :value (::tx/locked-until @tx-atom)
     :label (tr :tx/locked-until)}]
   [ui/account-select
    {:label (tr :tx/account)
     :on-change #(swap! tx-atom (fn [tx] (assoc tx ::tx/from %)))
     :value (::tx/from @tx-atom)
     :choices (accounts-vec ::a/private-key)
     }]
   [ui/account-select
    {:label (tr :tx/validator)
     :on-change #(swap! tx-atom (fn [tx] (assoc tx ::tx/validator-address %)))
     :value (::tx/validator-address @tx-atom)
     :choices (accounts-vec)
     }]])

(defn vote-tx-form [tx-atom]
  [:div
   [ui/account-select
    {:label (tr :tx/validator)
     :on-change #(swap! tx-atom (fn [tx] (assoc tx ::tx/from %)))
     :value (::tx/from @tx-atom)
     :choices (accounts-vec ::a/private-key)
     }]
   [amount-field
    {:on-change #(swap! tx-atom (fn [tx] (assoc-in tx [::tx/choices ::tx/fee-per-tx] %)))
     :value (get-in @tx-atom [::tx/choices ::tx/fee-per-tx])
     :label (tr :tx/fee-per-tx)
     }]
   [amount-field
    {:on-change #(swap! tx-atom (fn [tx] (assoc-in tx [::tx/choices ::tx/fee-per-256b] %)))
     :value (get-in @tx-atom [::tx/choices ::tx/fee-per-256b])
     :label (tr :tx/fee-per-256-bytes)
     }]
   [amount-field
    {:on-change #(swap! tx-atom (fn [tx] (assoc-in tx [::tx/choices ::tx/fee-per-account-day] %)))
     :value (get-in @tx-atom [::tx/choices ::tx/fee-per-account-day])
     :label (tr :tx/fee-per-account-day)
     }]
   [ant/form-item
    (merge ui/item-style
           {:label (tr :tx/protocol)})
    [ant/input-number
     {:value (get-in @tx-atom [::tx/choices ::types/protocol])
      :on-change #(swap! tx-atom (fn [tx] (assoc-in tx [::tx/choices ::types/protocol] %)))
      :min 0
      :max 255
      :disabled false}]]
   [ui/timestamp-field
    {:on-change #(swap! tx-atom (fn [tx] (assoc tx ::tx/timestamp %)))
     :value (::tx/timestamp @tx-atom)
     :predefined-values [[(tr :tx/now) timestamp/now]]}]])

(defn send-tx [tx-atom status-atom]
  (reset! status-atom :submitting)
  (go
    (let [tx @tx-atom
          response (<! (tendermint/request
                        @rpc-endpoint
                        "broadcast_tx_commit"
                        {:tx (str "0x" (hex/encode (clj->js (tx/generate (get @accounts (::tx/from tx)) tx))))}))]
      (when response
        (let [check-tx-code (get-in response [:check_tx :code] 0)
              code (if (zero? check-tx-code)
                     (get-in response [:deliver_tx :code] 0)
                     check-tx-code)]
          (if (zero? code)
            (do (reset! status-atom :success)
                (reset! tx-atom (tx/template :transfer))
                (ant/message-success (tr :tx/tx-committed)))
            (do (reset! status-atom :failure)
                (ant/message-error (str (tr :tx/error) ": "
                                        (tendermint/error-description code)))
                )))))))

(defn transaction-form []
  (let [tx (r/atom (tx/template :transfer))
        status (r/atom :none)
        types (array-map :transfer transfer-tx-form
                         :lock lock-tx-form
                         :vote vote-tx-form
                         )]
    (fn []
      [:div
       [ant/form
        [ant/form-item
         (merge ui/item-style
                {:label (tr :tx/type)})
         [ant/select
          {:on-change #(reset! tx (tx/template (keyword %)))
           :value (name (::tx/type @tx))}
          (for [[type _] types]
            [ant/select-option
             {:key (name type)}
             type])]]]
       [(get types (::tx/type @tx)) tx]
       [ant/form-item
        {:wrapper-col {:offset ui/label-width}}
        [ant/button
         {:type "primary"
          :disabled (or (= @status :submitting)
                        (not (s/valid? ::tx/tx @tx)))
          :on-click #(send-tx tx status)}
         (tr :tx/send)]
        [ant/button
         {:disabled (or
                     (= @status :submitting)
                     (not (s/valid? ::tx/tx @tx)))
          :on-click (fn []
                      (let [modal-visible (r/atom true)
                            from (get @accounts (::tx/from @tx))]
                        (r/as-element
                         (ant/modal-info
                          {:title (tr :tx/transaction-binary)
                           :content (r/as-element
                                     [:code (binary-encode (tx/generate from @tx))])
                           :on-ok (fn [] (reset! modal-visible false))}))))}
         (tr :tx/show)]
        (when (= @status :submitting)
          [ant/icon
           {:type "loading"}])]])))

(defn new-account-form []
  (let [visible (r/atom (empty? @accounts))
        account (r/atom {})]
    (fn []
      [:div
       (when @visible
         [ant/modal
          {:visible @visible
           :title (tr :accounts/add-account)
           :ok-text (tr :accounts/add)
           :on-ok #(when (::a/label @account)
                     (do
                       (put-account! (a/add-keypair @account))
                       (reset! account {})
                       (reset! visible false)))
           :on-cancel #(do (reset! account {})
                           (reset! visible false))}
          [:p (tr :accounts/new-account-generated-when-no-addresses)]
          [:p (tr :accounts/watch-only-when-address-only)]
          [:p (tr :accounts/address-calculated-when-only-private-key)]
          [:p (tr :accounts/empty-label-prohibition)]
          [:p (tr :accounts/merkle-proofs-not-supported)]
          [ant/form-item
           (merge ui/item-style
                  {:label (tr :accounts/label)})
           [ant/input
            {:type "text"
             :value (::a/label @account)
             :on-change (fn [e] (swap! account #(assoc % ::a/label (-> e .-target .-value))))}]]
          [ant/form-item
           (merge ui/item-style
                  {:label (tr :accounts/address)})
           [ant/input
            {:type :text
             :value (binary-encode (::a/address @account))
             :on-change (fn [e] (swap! account
                                       #(assoc % ::a/address (->
                                                              e
                                                              .-target
                                                              .-value
                                                              binary-decode))))}]]
          [ant/form-item
           (merge ui/item-style
                  {:label (tr :accounts/private-key)})
           [ant/input
            {:type :text
             :value (binary-encode (::a/private-key @account))
             :on-change (fn [e] (swap! account #(assoc % ::a/private-key
                                                       (->
                                                        e
                                                        .-target
                                                        .-value
                                                        binary-decode))))}]]])
       [ant/button
        {:icon :plus
         :on-click #(reset! visible true)}
        (tr :accounts/add)]])))

(defn tx-search [query page]
  (go (reset! txs (<! (tendermint/tx-search @rpc-endpoint query page)))))

(defn tx-page-input [page-atom]
  [ant/form-item
   (merge ui/item-style
          {:label (tr :tx/page)})
   [ant/input-number
    {:min 1
     :value @page-atom
     :on-change #(reset! page-atom (int %))}]])

(defn transaction-search-form []
  (let [from-address (r/atom nil)
        to-address (r/atom nil)
        hash (r/atom nil)
        height (r/atom nil)
        page (r/atom 1)]
    (fn []
      [ant/form
       [ui/account-select
        {:allow-clear true
         :label (tr :tx/from)
         :value @from-address
         :on-change #(reset! from-address %)
         :choices (accounts-vec)
         }]
       [ui/account-select
        {:allow-clear true
         :label (tr :tx/to)
         :value @to-address
         :on-change #(reset! to-address %)
         :choices (accounts-vec)
         }]
       [ant/form-item
        (merge ui/item-style
               {:label (tr :tx/hash)})
        [ant/input
         {:type :text
          :default-value (binary-encode @hash)
          :on-change #(reset! hash
                              (-> %
                                  .-target
                                  .-value
                                  binary-decode))}]]
       [ant/form-item
        (merge ui/item-style
               {:label (tr :tx/block-height)})
        [ant/input-number
         {:min 1
          :value @height
          :on-change #(reset! height (if (string? %)
                                       (int %)
                                       %))}]]
       [tx-page-input page]
       [ant/form-item
        {:wrapper-col {:offset ui/label-width}}
        [ant/button
         {:on-click #(tx-search {"tx.from" (converter/encode binary/base64 @from-address)
                                 "tx.to" (converter/encode binary/base64 @to-address)
                                 "tx.height" @height
                                 "tx.hash" (converter/encode binary/hex @hash)}
                                @page)}
         (tr :tx/search)]]])))

(defrecord Section [key name content])

(def sections
  (list
   (Section.
    "accounts"
    (tr :sections/accounts)
    [:div
     [accounts-refresh-button]
     [export-backup-button]
     [import-backup-button]
     [accounts-table]
     [new-account-form]])
   (Section.
    "create-transaction"
    (tr :sections/create-transaction)
    [transaction-form])
   (Section.
    "transactions"
    (tr :sections/transactions)
    [:div
     [transaction-search-form]
     [txs-table]])
   (Section.
    "settings"
    (tr :sections/settings)
    [ant/form
     [rpc-endpoint-field]
     [binary-encoding-field]
     [message-encoding-field]
     [unit-field]])
   ))

(defn drawer []
  [ant/menu
   {:on-select #(reset! active-section (.-key %))
    :default-selected-keys [@active-section]
    :theme "dark"}
   (for [section sections]
     ^{:key (:key section)}
     [ant/menu-item
      {:key (:key section)}
      (:name section)])])

(defn main []
  [:main
   (let [active-section-deref @active-section]
     (for [section sections]
       (when (= active-section-deref (:key section))
         ^{:key (:key section)}
         [:section
          (:content section)])))])

(defn modal-terms []
  (ant/modal-confirm
   {:title (tr :terms/title)
    :content (r/as-element
              [:div
               (when @accepted-terms-checksum
                 [:p [:strong (tr :terms/text-changed-reevaluate)]])
               (when (not= (tr :terms/terms) terms/terms)
                 [:div
                  [:hr]
                  [:p (tr :terms/l10n-convenience-only)]
                  (tr :terms/terms)
                  [:hr]])
               terms/terms])
    :mask-closable false
    :ok-text (tr :terms/i-agree)
    :cancel-text (tr :terms/i-dont-agree)
    :on-ok #(do (reset! accepted-terms-checksum terms/terms-checksum)
                false)
    :on-cancel #(identity :not-false-dont-close)
    }
   ))

(defn home-page []
  [ant/layout
   [ant/layout-sider
    [drawer]]
   [ant/layout
    [ant/layout-content
     [main]]
    [ant/layout-footer
     (tr :terms/footer)]]])

;; Initialize app

(defn mount-root []
  (r/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root)
  (when-not (= @accepted-terms-checksum terms/terms-checksum)
    (modal-terms)))
