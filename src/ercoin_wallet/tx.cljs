(ns ercoin-wallet.tx
  (:require
   [clojure.spec.alpha :as s]
   [cljsjs.nacl-fast]
   [ercoin-wallet.account :as a]
   [ercoin-wallet.binary :as binary]
   [ercoin-wallet.converter :as converter]
   [ercoin-wallet.uint :as uint]
   [ercoin-wallet.types :as t]
   [goog.crypt.base64 :as base64]
  ))

(s/def ::type #{:transfer :lock :vote :burn})
(s/def ::from ::t/address)
(s/def ::to ::t/address)
(s/def ::validator-address ::t/address)
(s/def ::timestamp ::t/timestamp)
(s/def ::valid-until ::t/timestamp)
(s/def ::locked-until ::t/timestamp)
(s/def ::value ::t/value)
(s/def ::message (s/coll-of ::t/byte :kind vector? :max-count 255))
(s/def ::fee-per-256b ::t/value)
(s/def ::fee-per-account-day ::t/value)
(s/def ::fee-per-tx ::t/value)
(s/def ::choices (s/keys :req [::t/protocol ::fee-per-256b ::fee-per-account-day ::fee-per-tx]))
;; Since we don’t currently validate signatures, we can treat them as opaque and define them as arbitrary binaries.
(s/def ::signature (s/coll-of ::t/byte :kind vector? :min-count 97))

(defmulti tx-type ::type)
(defmethod tx-type :transfer [_]
  (s/keys :req [::type ::timestamp ::to ::value ::message]
          :opt [::signature]))
(defmethod tx-type :lock [_]
  (s/keys :req [::type ::locked-until ::validator-address]
          :opt [::signature]))
(defmethod tx-type :vote [_]
  (s/keys :req [::type ::timestamp ::choices]
          :opt [::signature]))
(defmethod tx-type :burn [_]
  (s/keys :req [::type ::timestamp ::value ::message]
          :opt [::signature]))

(s/def ::tx (s/multi-spec tx-type ::type))

(defn template [tx-type]
  (merge {::type tx-type}
         (case tx-type
           :transfer {::message []
                      ::value 0}
           :vote {::choices {::t/protocol 1
                             ::fee-per-tx 0
                             ::fee-per-256b 0
                             ::fee-per-account-day 0}}
           {})))

(defn type-no->type [number]
  (case number
    0 :transfer
    2 :lock
    3 :vote
    4 :burn))

(s/fdef type->type-no
  :args (s/cat :type ::type)
  :ret int?)
(defn type->type-no [type]
  (case type
    :transfer 0
    :lock 2
    :vote 3
    :burn 4))

(s/fdef normalize
  :args (s/cat :tx ::tx)
  :ret ::tx)
(defn normalize [tx]
  (if (empty? (::signature tx))
    (dissoc tx ::signature)
    tx))

(s/fdef choices-deserialize
  :args (s/cat :binary ::t/binary)
  :ret ::choices)
(defn choices-deserialize [binary]
  {::fee-per-tx (uint/vec->uint (subvec binary 0 8))
   ::fee-per-256b (uint/vec->uint (subvec binary 8 16))
   ::fee-per-account-day (uint/vec->uint (subvec binary 16 24))
   ::t/protocol (nth binary 24)})

(s/fdef choices-serialize
  :args (s/cat :choices ::choices)
  :ret ::t/binary)
(defn choices-serialize [choices]
  (concat
   (uint/uint->vec (::fee-per-tx choices) 8)
   (uint/uint->vec (::fee-per-256b choices) 8)
   (uint/uint->vec (::fee-per-account-day choices) 8)
   [(::t/protocol choices)]))

(defmulti deserialize-1 #(type-no->type (first %)))
(defmethod deserialize-1 :transfer [binary]
  (let [msg-length (nth binary 45)]
    {::type :transfer
     ::timestamp (uint/vec->uint (subvec binary 1 5))
     ::to (subvec binary 5 37)
     ::value (uint/vec->uint (subvec binary 37 45))
     ::message (subvec binary 46 (+ 46 msg-length))
     ::signature (subvec binary (+ 46 msg-length))}))
(defmethod deserialize-1 :lock [binary]
  {::type :lock
   ::locked-until (uint/vec->uint (subvec binary 1 5))
   ::validator-address (subvec binary 5 37)
   ::signature (subvec binary 37)})
(defmethod deserialize-1 :vote [binary]
  {::type :vote
   ::timestamp (uint/vec->uint (subvec binary 1 5))
   ::choices (choices-deserialize (subvec binary 5 30))
   ::signature (subvec binary 30)})
(defmethod deserialize-1 :burn [binary]
  (let [msg-length (nth binary 13)]
    {::type :burn
     ::timestamp (uint/vec->uint (subvec binary 1 5))
     ::value (uint/vec->uint (subvec binary 5 13))
     ::message (subvec binary 14 (+ 14 msg-length))
     ::signature (subvec binary (+ 14 msg-length))}))

(s/fdef deserialize
  :args (s/cat :binary ::t/binary)
  :ret ::tx)
(defn deserialize [binary]
  (-> binary
      (deserialize-1)
      (normalize)))

(defn get-tag [key_ tx-info]
  (->> tx-info
       :tx_result
       :events
       (filter #(= (:type %) "tx"))
       first
       :attributes
       (filter #(= (:key %)
                   (converter/encode binary/base64 (uint/str->utf8 key_))))
       first
       :value
       ;; Why does Tendermint perform Base64 encoding on strings?
       base64/decodeString))

(defn from-rpc-info [tx-info]
  (with-meta
    (deserialize (converter/decode binary/base64 (:tx tx-info)))
    {::fee (js/parseInt (get-tag "fee" tx-info))
     ::from (converter/decode binary/base64 (get-tag "from" tx-info))
     ::hash (converter/decode binary/hex (:hash tx-info))
     ::height (js/parseInt (:height tx-info))}))

(defmulti serialize-1 ::type)
(defmethod serialize-1 :transfer [{:keys [::timestamp ::to ::value ::message]}]
  [(uint/uint->vec timestamp 4)
   to
   (uint/uint->vec value 8)
   (count message)
   message])
(defmethod serialize-1 :lock [{:keys [::locked-until ::validator-address]}]
  [(uint/uint->vec locked-until 4)
   validator-address])
(defmethod serialize-1 :vote [{:keys [::timestamp ::choices]}]
  [(uint/uint->vec timestamp 4)
   (choices-serialize choices)])
(defmethod serialize-1 :burn [{:keys [::timestamp ::value ::message]}]
  [(uint/uint->vec timestamp 4)
   (uint/uint->vec value 8)
   (count message)
   message])

(s/fdef serialize
  :args (s/cat :tx ::tx)
  :ret ::t/binary)
(defn serialize [tx]
  (vec (concat
        [(type->type-no (::type tx))]
        (flatten (serialize-1 tx))
        (::signature tx))))

(defn sign-ed25519-detached [binary account]
  (-> (js/nacl.sign.detached (js/Uint8Array.from (clj->js binary))
                             (js/Uint8Array.from (clj->js (::a/private-key account))))
      js/Array.from
      js->clj))

(defn sign [binary account]
  (let [to-sign (vec (concat binary
                             [1]
                             ;; Currently we support only simple accounts, so address is public key.
                             (::a/address account)))]
    (vec (concat to-sign
                 (sign-ed25519-detached to-sign account)))))

(defn generate [from incomplete-tx]
  (sign (serialize incomplete-tx) from))
