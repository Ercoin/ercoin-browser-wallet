(ns ercoin-wallet.types
  (:require
   [clojure.spec.alpha :as s]))

(s/def ::byte (s/int-in 0 256))
(s/def ::binary (s/coll-of ::byte :kind vector?))
(s/def ::address (s/coll-of ::byte :kind vector? :count 32))
(s/def ::value (s/int-in 0 18446744073709551615))
(s/def ::timestamp (s/int-in 0 4294967295))
(s/def ::protocol (s/int-in 0 255))
